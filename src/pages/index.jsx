import React from "react";
import { Link } from "gatsby";
import styled, { ThemeProvider } from "styled-components";
import "typeface-roboto";
import { Helmet } from "react-helmet";

import GlobalStyle from "../styles/GlobalStyle";
import theme from "../styles/theme";

import Image from "../components/image";
import SEO from "../components/seo";
import SlideSection from "../components/SlideSection";
import logo from "../assets/logo.svg";

import SlideOne from "../slides/slide-1/SlideOne";
import SlideTwo from "../slides/slide-2/SlideTwo";
import SlideThree from "../slides/slide-3/SlideThree";
import SlideFour from "../slides/slide-4/SlideFour";
import SlideFive from "../slides/slide-5/SlideFive";
import SlideSix from "../slides/slide-6/SlideSix";
import SlideSeven from "../slides/slide-7/SlideSeven";
import SlideEight from "../slides/slide-8/SlideEight";
import SlideNine from "../slides/slide-9/SlideNine";
import SlideTen from "../slides/slide-10/SlideTen";
import SlideEleven from "../slides/slide-11/SlideEleven";

const StyledIndex = styled.div`
  overflow-y: scroll;
  height: 100vh;
  scroll-snap-points-y: repeat(100vh);
  scroll-snap-type: y proximity;

  @media (max-width: ${p => p.theme.breakpoint.desktopM}) {
    scroll-snap-type: y proximity;
  }
`;

const IndexPage = () => (
  <ThemeProvider theme={theme}>
    <StyledIndex>
      <Helmet>
        <meta charSet="utf-8" />
        <title>Likha+ VFX</title>
      </Helmet>
      
      <GlobalStyle></GlobalStyle>

      <SlideOne></SlideOne>
      <SlideTwo></SlideTwo>
      <SlideThree></SlideThree>
      <SlideFour></SlideFour>
      <SlideFive></SlideFive>
      <SlideSix></SlideSix>
      <SlideSeven></SlideSeven>
      <SlideEight></SlideEight>
      <SlideNine></SlideNine>
      <SlideTen></SlideTen>
      <SlideEleven></SlideEleven>
    </StyledIndex>
  </ThemeProvider>
);

export default IndexPage;
