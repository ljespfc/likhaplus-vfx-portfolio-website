import React from "react";
import styled, { css } from "styled-components";
import Fade from "react-reveal/Fade";

const StyledSlideSection = styled.section`
  background-color: ${p =>
    (p.color === "primary" && p.theme.color.primary.main) ||
    (p.color === "secondary" && p.theme.color.secondary.main) ||
    (p.color === "white" && p.theme.color.white)};

  color: ${p =>
    (p.color === "primary" && p.theme.color.basic.white) ||
    (p.color === "secondary" && p.theme.color.grey.dark) ||
    (p.color === "white" && p.theme.color.grey.dark)};
  min-height: 100%;
  scroll-snap-align: start;
  position: relative;

  padding-bottom: ${p => p.theme.size.xl} !important;

  @media (max-width: ${p => p.theme.breakpoint.tabletLandscape}) {
    min-height: 0;
  } 
 
  ${p =>
    p.peekColor &&
    css`
      border-bottom: ${p => p.theme.size.l} solid
        ${p =>
          (p.peekColor === "primary" && p.theme.color.primary.main) ||
          (p.peekColor === "secondary" && p.theme.color.secondary.main) ||
          (p.peekColor === "white" && p.theme.color.white)};
    `}

  
  @media (max-width: ${p => p.theme.breakpoint.tabletLandscape}) {
    height: unset;
    scroll-snap-align: none;
  }

  ${p =>
    p.layout === "titleAndContent" &&
    css`
      padding: ${p => p.theme.size.xl} ${p => p.theme.size.increment(12)};
      @media (max-width: ${p => p.theme.breakpoint.tabletLandscape}) {
        padding: ${p => p.theme.size.xl};
      }

      @media (max-width: ${p => p.theme.breakpoint.tabletPortrait}) {
        padding: ${p => p.theme.size.l};
      }
    `}

    .title {
      ${p => p.theme.typographyMixin.headline1};
      margin-bottom: ${p => p.theme.size.xl};

      @media (max-width: ${p => p.theme.breakpoint.tabletLandscape}) {
        margin-bottom: ${p => p.theme.size.l};
      } 
    }

`;

export default function SlideSection({ children, title, ...rest }) {
  return (
    <StyledSlideSection {...rest}>
      <Fade bottom cascade>
        <div>
          {title && <h1 className="title">{title}</h1>}

          {children}
        </div>
      </Fade>
    </StyledSlideSection>
  );
}
