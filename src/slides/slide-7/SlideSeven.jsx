import React from "react";
import styled from "styled-components";
import { useSpring, useTrail, animated } from "react-spring";

import mattePainting1Image from "./matte-painting-01.jpg";
import mattePainting2Image from "./matte-painting-02.jpg";
import mattePainting3Image from "./matte-painting-03.jpg";
import mattePainting4Image from "./matte-painting-04.jpg";
import mattePaintingVideo from "./mattePaintingVideo.webm";

import SlideSection from "../../components/SlideSection";

const StyledSlideSection = styled(SlideSection)`
  .wrapper {
    display: grid;
    grid-template-columns: 3fr 1fr 1fr;
    grid-template-rows: auto auto;
    grid-gap: ${p => p.theme.size.m};

    @media (max-width: ${p => p.theme.breakpoint.desktopM}) {
      grid-template-columns: auto;
    }

    div {
      height: 100%;
    }

    img {
      object-fit: cover;
      width: 100%;
      height: 100%;
      box-shadow: ${p => p.theme.shadow[2]};
    }

    .video-wrapper {
      grid-row: 1/3;
      display: flex;
      align-items: center;
    }

    video {
      display: block;
      width: 100%;
      height: 100%;
      object-fit: cover;
      box-shadow: ${p => p.theme.shadow[2]};
    }
  }
`;

const SlideSeven = () => {
  return (
    <StyledSlideSection
      color="primary"
      layout="titleAndContent"
      title="Matte Painting"
    >
      <div className="wrapper">
        <div className="video-wrapper">
          <video src={mattePaintingVideo} autoPlay muted loop></video>
        </div>

        <div>
          <img src={mattePainting1Image} alt="" />
        </div>

        <div>
          <img src={mattePainting2Image} alt="" />
        </div>

        <div>
          <img src={mattePainting3Image} alt="" />
        </div>

        <div>
          <img src={mattePainting4Image} alt="" />
        </div>
      </div>
    </StyledSlideSection>
  );
};

export default SlideSeven;
