import React from "react";
import styled from "styled-components";
import { useSpring, useTrail, animated } from "react-spring";

import logo from "../../assets/logo.svg";
import SlideSection from "../../components/SlideSection";
import ScrollDown from "../../components/ScrollDown";

const StyledSlideSection = styled(SlideSection)`
  display: flex;
  justify-content: center;
  align-items: center;

  .wrapper {
    display: flex;
    flex-flow: column;
    justify-content: center;
    align-items: center;
    height: 100%;
  }

  @media (max-width: ${p => p.theme.breakpoint.tabletLandscape}) {
    height: 100%;
    padding: ${p => p.theme.size.xl};
  }

  @media (max-width: ${p => p.theme.breakpoint.tabletPortrait}) {
    padding: ${p => p.theme.size.m};
  }

  .logo {
    margin-bottom: ${p => p.theme.size.l};
    opacity: 0;

    @media (max-width: ${p => p.theme.breakpoint.tabletLandscape}) {
      width: 62vw;
    }
  }

  .tagline-group {
    display: flex;
    font-size: ${p => p.theme.typography.scale.display2};
    color: ${p => p.theme.color.grey.medium};
    text-align: center;

    *:not(:last-child) {
      margin-right: ${p => p.theme.size.l};
    }

    @media (max-width: ${p => p.theme.breakpoint.tabletPortrait}) {
      font-size: ${p => p.theme.typography.scale.base};

      *:not(:last-child) {
        margin-right: ${p => p.theme.size.m};
      }
    }

    @media (max-width: ${p => p.theme.breakpoint.tabletLandscape}) {
      *:not(:last-child) {
        margin-right: ${p => p.theme.size.m};
      }
    }
  }
`;

export default function SlideOne() {
  const logoAnimation = useSpring({
    to: { opacity: 1 },
    from: { opacity: 0 },
    delay: 500,
  });

  const taglineItems = ["We Define.", "We Create.", "We Deliver."];
  const taglineAnimation = useTrail(taglineItems.length, {
    from: { opacity: 0, transform: "translateY(100%)" },
    to: { opacity: 1, transform: "translateY(0%)" },
    delay: 1000,
  });

  return (
    <StyledSlideSection color="white" peekColor="primary">
      <div className="wrapper">
        <animated.img
          style={logoAnimation}
          className="logo"
          src={logo}
          alt=""
        />

        <div className="tagline-group">
          {taglineAnimation.map((props, index) => (
            <animated.span key={index} style={props}>
              {taglineItems[index]}
            </animated.span>
          ))}
        </div>
      </div>
      
      <ScrollDown></ScrollDown>
    </StyledSlideSection>
  );
}
