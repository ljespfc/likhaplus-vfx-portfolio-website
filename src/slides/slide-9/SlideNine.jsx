import React from "react";
import styled from "styled-components";

import motionGraphics1Image from "./motion-graphics-01.jpg";
import motionGraphics2Image from "./motion-graphics-02.jpg";
import motionGraphicsVideo from "./motion-graphics-video.mp4";

import SlideSection from "../../components/SlideSection";

const StyledSlideSection = styled(SlideSection)`
  .wrapper {
    display: grid;
    grid-template-columns: 2fr 1fr;
    grid-template-rows: auto auto;
    grid-gap: ${p => p.theme.size.m};

    @media (max-width: ${p => p.theme.breakpoint.desktopM}) {
      grid-template-columns: auto;
    }

    div {
      height: 100%;
    }

    img {
      object-fit: cover;
      width: 100%;
      height: 100%;
      box-shadow: ${p => p.theme.shadow[2]};
    }

    .video-wrapper {
      grid-row: 1/3;
      display: flex;
      align-items: center;
    }

    video {
      display: block;
      width: 100%;
      height: 100%;
      object-fit: cover;
      box-shadow: ${p => p.theme.shadow[2]};
    }
  }
`;

const SlideNine = () => {
  return (
    <StyledSlideSection
      color="primary"
      layout="titleAndContent"
      title="Motion Graphics"
    >
      <div className="wrapper">
        <div className="video-wrapper">
          <video src={motionGraphicsVideo} autoPlay muted loop></video>
        </div>

        <div>
          <img src={motionGraphics1Image} alt="" />
        </div>

        <div>
          <img src={motionGraphics2Image} alt="" />
        </div>
      </div>
    </StyledSlideSection>
  );
};

export default SlideNine;
