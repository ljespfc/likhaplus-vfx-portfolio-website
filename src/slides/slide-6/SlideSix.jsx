import React from "react";
import styled from "styled-components";
import { useSpring, useTrail, animated } from "react-spring";

import rendering1Image from "./rendering-01.jpg";
import rendering2Image from "./rendering-02.jpg";
import rendering3Image from "./rendering-03.jpg";
import rendering4Image from "./rendering-04.jpg";
import rendering5Image from "./rendering-05.jpg";

import SlideSection from "../../components/SlideSection";

const StyledSlideSection = styled(SlideSection)`
  .wrapper {
    display: grid;
    grid-template-columns: auto auto auto;
    grid-template-rows: auto auto;
    grid-gap: ${p => p.theme.size.m};

    @media (max-width: ${p => p.theme.breakpoint.desktopM}) {
      grid-template-columns: auto;
    }

    div {
      height: 100%;
      box-shadow: ${p => p.theme.shadow[2]};
    }

    img {
      object-fit: cover;
      width: 100%;
      height: 100%;
    }
  }

  .center-wrapper {
    grid-column: 2/3;
    grid-row: 1/3;

    @media (max-width: ${p => p.theme.breakpoint.desktopM}) {
      grid-column: unset;
      grid-row: unset;
  }
`;

const SlideSix = () => {
  return (
    <StyledSlideSection
      color="primary"
      layout="titleAndContent"
      title="Rendering"
    >
      <div className="wrapper">
        <div>
          <img src={rendering1Image} alt="" />
        </div>
        <div>
          <img src={rendering2Image} alt="" />
        </div>
        <div className="center-wrapper">
          <img src={rendering3Image} alt="" />
        </div>
        <div>
          <img src={rendering4Image} alt="" />
        </div>
        <div>
          <img src={rendering5Image} alt="" />
        </div>
      </div>
    </StyledSlideSection>
  );
};

export default SlideSix;
