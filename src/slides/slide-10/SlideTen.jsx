import React from "react";
import styled from "styled-components";

import SlideSection from "../../components/SlideSection";

const StyledSlideSection = styled(SlideSection)`
  .list {
    margin-left: ${p => p.theme.size.m};
    ${p => p.theme.typographyMixin.headline2};

    li:not(:last-child) {
      margin-bottom: ${p => p.theme.size.m};
    }

    @media (max-width: ${p => p.theme.breakpoint.tabletPortrait}) {
      font-size: ${p => p.theme.typography.scale.display4};
    }
  }
`;

const SlideTen = () => {
  return (
    <StyledSlideSection
      color="primary"
      layout="titleAndContent"
      title="Services"
      peekColor="secondary"
    >
      <ul className="list">
        <li>Concept Art / Matte Painting</li>
        <li>Compositing</li>
        <li>3D Asset Building</li>
        <li>3D Animation/Simulation</li>
        <li>2D/3D Motion Tracking</li>
        <li>Rotoscoping</li>
        <li>3D Stereoscopic</li>
        <li>Motion Graphics / Title Design</li>
        <li>Product Rendering</li>
        <li>Real State Rendering</li>
      </ul>
    </StyledSlideSection>
  );
};

export default SlideTen;
