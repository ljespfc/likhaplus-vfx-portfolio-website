import React from "react";
import styled from "styled-components";

import fxAndSimulation1Image from "./fx-and-simulation-1.jpg";
import fxAndSimulation2Image from "./fx-and-simulation-2.jpg";
import fxAndSimulationVideo from "./fx-and-simulation-video.webm";

import SlideSection from "../../components/SlideSection";

const StyledSlideSection = styled(SlideSection)`
  .wrapper {
    display: grid;
    grid-template-columns: 2fr 1fr;
    grid-template-rows: auto auto;
    grid-gap: ${p => p.theme.size.m};

    @media (max-width: ${p => p.theme.breakpoint.desktopM}) {
      grid-template-columns: auto;
    }

    div {
      height: 100%;
    }

    img {
      object-fit: cover;
      width: 100%;
      height: 100%;
      box-shadow: ${p => p.theme.shadow[2]};
    }

    .video-wrapper {
      grid-row: 1/3;
      display: flex;
      align-items: center;
    }

    video {
      display: block;
      width: 100%;
      height: 100%;
      object-fit: cover;
      box-shadow: ${p => p.theme.shadow[2]};
    }
  }
`;

const SlideEight = () => {
  return (
    <StyledSlideSection
      color="primary"
      layout="titleAndContent"
      title="FX and Simulation"
    >
      <div className="wrapper">
        <div className="video-wrapper">
          <video src={fxAndSimulationVideo} autoPlay muted loop></video>
        </div>

        <div>
          <img src={fxAndSimulation1Image} alt="" />
        </div>

        <div>
          <img src={fxAndSimulation2Image} alt="" />
        </div>
      </div>
    </StyledSlideSection>
  );
};

export default SlideEight;
