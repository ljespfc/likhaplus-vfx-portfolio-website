import React from "react";
import styled from "styled-components";
import { useSpring, useTrail, animated } from "react-spring";

import icon1 from "./icon-1.png";
import icon2 from "./icon-2.png";
import icon3 from "./icon-3.png";

import SlideSection from "../../components/SlideSection";

const StyledSlideSection = styled(SlideSection)`
  color: ${p => p.theme.color.grey.dark};

  .why-us-block {
    display: flex;
    align-items: center;

    margin-bottom: ${p => p.theme.size.increment(6)};

    @media (max-width: ${p => p.theme.breakpoint.tabletPortrait}) {
      flex-flow: column;
      text-align: center;
    }
  }

  .why-us-image-wrapper {
    margin-right: ${p => p.theme.size.l};
    min-width: ${p => p.theme.size.increment(10)};
    text-align: center;

    @media (max-width: ${p => p.theme.breakpoint.tabletPortrait}) {
      margin-right: unset;
      margin-bottom: ${p => p.theme.size.baseHalf};
    }
  }

  .why-us-image {
    @media (max-width: ${p => p.theme.breakpoint.tabletPortrait}) {
      width: ${p => p.theme.size.increment(6)};
    }
  }

  .why-us-header {
    ${p => p.theme.typographyMixin.headline2};
    margin-bottom: ${p => p.theme.size.m};
  }

  .why-us-text {
    font-size: ${p => p.theme.typography.scale.display3};

    @media (max-width: ${p => p.theme.breakpoint.tabletPortrait}) {
      font-size: ${p => p.theme.typography.scale.body};
    }
  }
`;

const SlideThree = () => {
  return (
    <StyledSlideSection
      color="secondary"
      layout="titleAndContent"
      title="Why Choose Us?"
    >
      {/* <h1 className="heading">Why Us?</h1> */}

      <div className="why-us-block">
        <div className="why-us-image-wrapper">
          <img className="why-us-image" src={icon1} alt="" />
        </div>

        <div className="why-us-text-wrapper">
          <h2 className="why-us-header">We Define</h2>
          <p className="why-us-text">
            We work hard planning from the script to offer you the best
            solutions in every stage of the project.
          </p>
        </div>
      </div>

      <div className="why-us-block">
        <div className="why-us-image-wrapper">
          <img className="why-us-image" src={icon2} alt="" />
        </div>

        <div className="why-us-text-wrapper">
          <h2 className="why-us-header">We Create</h2>
          <p className="why-us-text">
            We make your shots look perfect, believable and coherent with your
            storytelling.
          </p>
        </div>
      </div>

      <div className="why-us-block">
        <div className="why-us-image-wrapper">
          <img className="why-us-image" src={icon3} alt="" />
        </div>

        <div className="why-us-text-wrapper">
          <h2 className="why-us-header">We Deliver</h2>
          <p className="why-us-text">
            We don’t want our client to worry. We deliver before the deadline.
          </p>
        </div>
      </div>
    </StyledSlideSection>
  );
};

export default SlideThree;
