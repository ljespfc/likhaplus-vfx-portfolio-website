import React from "react";
import styled from "styled-components";

import SlideSection from "../../components/SlideSection";

const StyledSlideSection = styled(SlideSection)`
  .video-wrapper-wrapper {
    display: flex;
    justify-content: center;
  }

  .video-wrapper {
    overflow: hidden;
    position: relative;
    width: 62%;

    @media (max-width: ${p => p.theme.breakpoint.desktopM}) {
      width: 100%;
    }
  }

  .video-wrapper::after {
    padding-top: 56.25%;
    display: block;
    content: "";
  }

  .video-wrapper iframe {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
  }
`;

const SlideFour = () => {
  return (
    <StyledSlideSection
      color="secondary"
      peekColor="primary"
      layout="titleAndContent"
      title="Showreel"
    >
      <div className="video-wrapper-wrapper">
        <div className="video-wrapper">
          <iframe
            src="https://www.youtube-nocookie.com/embed/_PfH4-WK3wA?rel=0"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen
          ></iframe>
        </div>
      </div>
    </StyledSlideSection>
  );
};

export default SlideFour;
