import React from "react";
import styled from "styled-components";

import SlideSection from "../../components/SlideSection";

const StyledSlideSection = styled(SlideSection)`
  padding: ${p => p.theme.size.xl};
  display: flex;
  justify-content: center;
  align-items: center;
  padding: ${p => p.theme.size.xl} ${p => p.theme.size.increment(12)};

  @media (max-width: ${p => p.theme.breakpoint.tabletLandscape}) {
    height: 100%;
  }

  @media (max-width: ${p => p.theme.breakpoint.tabletPortrait}) {
    padding: ${p => p.theme.size.l};
  }

  .text {
    font-size: 3rem;
    color: ${p => p.theme.color.basic.white};
    font-weight: 300;
    line-height: 1.5;

    @media (max-width: ${p => p.theme.breakpoint.tabletLandscape}) {
      font-size: ${p => p.theme.typography.scale.display2};
    }

    @media (max-width: ${p => p.theme.breakpoint.tabletPortrait}) {
      font-size: ${p => p.theme.typography.scale.display3};
    }
  }
`;

export default function SlideTwo() {
  return (
    <StyledSlideSection color="primary" peekColor="secondary">
      <p className="text">
        <span className="bold">Likha+ </span>
        is a creative company that works across the complete production process
        to help define and create visual effects to support our clients' stories
        within film, commercials and beyond.
      </p>
    </StyledSlideSection>
  );
}
