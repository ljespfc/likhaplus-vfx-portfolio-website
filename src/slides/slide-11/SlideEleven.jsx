import React from "react";
import styled from "styled-components";

import phoneImage from "./phone.png";
import emailImage from "./email.png";
import locationImage from "./location.png";
import logo from "../../assets/logo.svg";

import SlideSection from "../../components/SlideSection";

const StyledSlideSection = styled(SlideSection)`
  min-height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;

  .wrapper {
    height: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
    padding: ${p => p.theme.size.xl};

    @media (max-width: ${p => p.theme.breakpoint.tabletLandscape}) {
      padding: ${p => p.theme.size.l};
      padding-bottom: ${p => p.theme.size.baseDouble};
      flex-flow: column;
    }
  }

  .call-to-action {
    font-size: 3rem;
    margin-right: ${p => p.theme.size.xl};
    text-align: right;
    font-weight: 700;
 
    @media (max-width: ${p => p.theme.breakpoint.tabletLandscape}) {
      /* padding: ${p => p.theme.size.l}; */
      text-align: center;
      margin-right: 0;
      margin-bottom: ${p => p.theme.size.xl};
    }
  }

  .contact-group {
    margin-left: ${p => p.theme.size.xl};

    & > *:not(:last-child) {
      margin-bottom: ${p => p.theme.size.l};

      @media (max-width: ${p => p.theme.breakpoint.tabletLandscape}) {
        margin-bottom: ${p => p.theme.size.xl};
      }
    }

    @media (max-width: ${p => p.theme.breakpoint.tabletLandscape}) {
      margin-left: 0;
    }
  }

  .contact-item {
    display: flex;
    align-items: center;

    @media (max-width: ${p => p.theme.breakpoint.tabletLandscape}) {
      flex-flow: column;
    }
  }

  .contact-icon-wrapper {
    margin-right: ${p => p.theme.size.l};

    @media (max-width: ${p => p.theme.breakpoint.tabletLandscape}) {
      margin-right: 0;
      margin-bottom: ${p => p.theme.size.baseHalf};
    }
  }

  .contact-icon-image {
    width: ${p => p.theme.size.increment(4)};
  }

  .contact-item-text {
    ${p => p.theme.typographyMixin.headline2};
    text-align: left;

    @media (max-width: ${p => p.theme.breakpoint.tabletLandscape}) {
      text-align: center;
    }
  }
`;

const SlideEleven = () => {
  return (
    <StyledSlideSection color="secondary">
      <div className="wrapper">
        <p className="call-to-action">Contact us and let’s get started!</p>

        <div className="contact-group">
          <div className="contact-item">
            <div className="contact-icon-wrapper">
              <img className="contact-icon-image" src={phoneImage} alt="" />
            </div>

            <a className="contact-item-text" href="tel:+639177009133">
              +63 9177009133
            </a>
          </div>

          <div className="contact-item">
            <div className="contact-icon-wrapper">
              <img className="contact-icon-image" src={emailImage} alt="" />
            </div>

            <a className="contact-item-text" href="mailto:likha+@gmail.com">
              likha+@gmail.com
            </a>
          </div>

          <div className="contact-item">
            <div className="contact-icon-wrapper">
              <img className="contact-icon-image" src={locationImage} alt="" />
            </div>

            <a
              className="contact-item-text"
              href="https://www.google.com/maps/place/115+SE+Dao,+Marikina,+1800+Metro+Manila/@14.6483828,121.1165318,17z/data=!3m1!4b1!4m5!3m4!1s0x3397b9a3e8bd575d:0x65ca40bdad4b3993!8m2!3d14.6483776!4d121.1187205"
              target="_blank"
            >
              1810, 155 SE Dao, Marikina, 1800 Metro Manila
            </a>
          </div>
        </div>
      </div>
    </StyledSlideSection>
  );
};

export default SlideEleven;
