import React from "react";
import styled from "styled-components";

import slideFiveVideo from "./slide-five-video.mp4";
import petshopImage from "./petshop.jpg";
import lighthouseImage from "./lighthouse.jpg";
import characterImage from "./character.jpg";

import SlideSection from "../../components/SlideSection";
import LoopingImage from "../../components/LoopingImage";

const StyledSlideSection = styled(SlideSection)`
  .wrapper {
    display: grid;
    grid-template-columns: 2fr 1fr;
    grid-gap: ${p => p.theme.size.m};

    @media (max-width: ${p => p.theme.breakpoint.desktopM}) {
      grid-template-columns: auto;
      grid-template-rows: 1fr 1fr;
    }

    > * {
      width: 100%;
      box-shadow: ${p => p.theme.shadow[2]};
    }
  }

  .video {
    width: 100%;
    display: block;
  }

  .image {
    object-fit: cover;
    height: 100%;
  }
`;

const SlideFive = () => {
  return (
    <StyledSlideSection
      color="primary"
      layout="titleAndContent"
      title="3D Asset Building"
    >
      <div className="wrapper">
        <video
          className="video"
          src={slideFiveVideo}
          autoPlay
          muted
          loop
        ></video>

        {/* <img className="image" src={petshopImage} alt="" /> */}
        <LoopingImage
          imageSrcs={[petshopImage, lighthouseImage, characterImage]}
        ></LoopingImage>
      </div>
    </StyledSlideSection>
  );
};

export default SlideFive;
